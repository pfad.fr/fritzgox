package aha

import (
	"encoding/xml"
	"fmt"
	"strconv"
	"strings"
	"time"
)

type Thermostat struct {
	// you should use the TemperatureSensor instead of ActualTemp
	ActualTemp        ThermostatTemp `xml:"tist"`
	TargetTemp        ThermostatTemp `xml:"tsoll"`
	LowTemp           ThermostatTemp `xml:"absenk"`
	ComfortTemp       ThermostatTemp `xml:"komfort"`
	APILocked         bool           `xml:"lock"`
	DeviceLocked      bool           `xml:"devicelock"`
	ErrorCode         int            `xml:"errorcode"`
	WindowOpened      bool           `xml:"windowopenactiv"`
	WindowOpenedUntil UnixSecond     `xml:"windowopenactiveendtime"`
	BoostActive       bool           `xml:"boostactive"`
	BoostActiveUntil  UnixSecond     `xml:"boostactiveendtime"`
	BatteryLow        bool           `xml:"batterylow"`
	BatteryLevel      int            `xml:"battery"`
	NextChange        struct {
		At       UnixSecond     `xml:"endperiod"`
		WishTemp ThermostatTemp `xml:"tchange"`
	} `xml:"nextchange"`
	SummerActive  bool `xml:"summeractive"`
	HolidayActive bool `xml:"holidayactive"`
}

type ThermostatTemp float32

const ThermostatTempUndefined = ThermostatTemp(-1)
const ThermostatTempOff = ThermostatTemp(0)
const ThermostatTempOn = ThermostatTemp(37)

func (t ThermostatTemp) xmlString() string {
	switch t {
	case 0: // off
		return "253"
	case 37: // on
		return "254"
	case -1: // undefined
		return "255"
	default:
		return strconv.FormatFloat(float64(t*2), 'f', -1, 32)
	}
}

func (t *ThermostatTemp) UnmarshalXML(d *xml.Decoder, start xml.StartElement) error {
	var v uint8
	if err := d.DecodeElement(&v, &start); err != nil {
		return err
	}
	switch v {
	case 253: // off
		*t = 0
	case 254: // on
		*t = 37
	case 255: // undefined
		*t = -1
	default:
		*t = ThermostatTemp(float32(v) / 2)
	}

	return nil
}

func (t ThermostatTemp) String() string {
	return strconv.FormatFloat(float64(t), 'f', -1, 32)
}

// SetThermostatTargetTemp
func (c *Client) SetThermostatTargetTemp(identifier string, t ThermostatTemp) error {
	u := c.joinPath("webservices", "homeautoswitch.lua").
		Param("switchcmd", "sethkrtsoll").
		Param("ain", identifier).
		Param("param", t.xmlString())
	return c.get(u.String(), nil)
}

// SetThermostatBoost
func (c *Client) SetThermostatBoost(identifier string, d time.Duration) error {
	endtimestamp := "0"
	if d > 0 {
		endtimestamp = strconv.FormatInt(time.Now().Add(d).Unix(), 10)
	}
	u := c.joinPath("webservices", "homeautoswitch.lua").
		Param("switchcmd", "sethkrboost").
		Param("ain", identifier).
		Param("endtimestamp", endtimestamp)
	var got string
	err := c.get(u.String(), &got)
	if err != nil {
		return err
	}
	if strings.TrimSpace(got) != endtimestamp {
		return fmt.Errorf("unexpected response, got %q, expected %q", got, endtimestamp)
	}
	return nil
}
