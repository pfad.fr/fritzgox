package aha

import (
	"testing"

	"gotest.tools/v3/assert"
)

// https://avm.de/fileadmin/user_upload/Global/Service/Schnittstellen/AVM_Technical_Note_-_Session_ID_english_2021-05-03.pdf
func TestChallengeResponse(t *testing.T) {
	// MD5 example
	challenge := "1234567z"
	password := "äbc"
	response, err := challengeResponse(challenge, password)
	assert.NilError(t, err)
	assert.Equal(t, "1234567z-9e224a41eeefa284df7bb0f26c2913e2", response)

	// PBKDF2 example
	challenge = "2$10000$5A1711$2000$5A1722"
	password = "1example!"
	response, err = challengeResponse(challenge, password)
	assert.NilError(t, err)
	assert.Equal(t, "5A1722$1798a1672bca7c6463d6b245f82b53703b0f50813401b03e4045a5861e689adb", response)
}
