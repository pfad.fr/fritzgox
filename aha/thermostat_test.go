package aha

import (
	"net/http"
	"strconv"
	"testing"
	"time"

	"gotest.tools/v3/assert"
)

func TestSetThermostatTargetTemp(t *testing.T) {
	called := 0
	params := []string{"17", "253", "254"}
	server := newServer(t, map[string]http.HandlerFunc{
		"sethkrtsoll": func(w http.ResponseWriter, r *http.Request) {
			t.Log(r.URL.String())
			q := r.URL.Query()
			assert.Equal(t, "123", q.Get("ain"))
			assert.Equal(t, params[called], q.Get("param"))
			called++
		},
	})
	addr := "http://" + server.Listener.Addr().String()

	c, err := NewClient(addr, "s3cr3t")
	assert.NilError(t, err)
	err = c.SetThermostatTargetTemp("123", 8.5)
	assert.NilError(t, err)

	err = c.SetThermostatTargetTemp("123", ThermostatTempOff)
	assert.NilError(t, err)

	err = c.SetThermostatTargetTemp("123", ThermostatTempOn)
	assert.NilError(t, err)
	assert.Equal(t, 3, called)
}

func TestSetThermostatBoost(t *testing.T) {
	called := 0
	first := time.Now().Add(time.Hour).Unix()

	params := []string{strconv.FormatInt(first, 10), "0"}
	server := newServer(t, map[string]http.HandlerFunc{
		"sethkrboost": func(w http.ResponseWriter, r *http.Request) {
			t.Log(r.URL.String())
			q := r.URL.Query()
			assert.Equal(t, "123", q.Get("ain"))
			assert.Equal(t, params[called], q.Get("endtimestamp"))
			called++
			w.Write([]byte(q.Get("endtimestamp")))
		},
	})
	addr := "http://" + server.Listener.Addr().String()

	c, err := NewClient(addr, "s3cr3t")
	assert.NilError(t, err)
	err = c.SetThermostatBoost("123", time.Hour)
	assert.NilError(t, err)

	err = c.SetThermostatBoost("123", 0)
	assert.NilError(t, err)

	assert.Equal(t, 2, called)
}
