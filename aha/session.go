// package aha allows to interact with the AVM Home Automation HTTP Interface (AHA), to control the connected smart devices.
package aha

import (
	"crypto/tls"
	"crypto/x509"
	"encoding/xml"
	"errors"
	"fmt"
	"net/http"
	"net/url"
	"sync"
	"time"
)

type Client struct {
	URL       url.URL
	Transport *http.Transport
	// if Username is empty, it will be auto-discovered
	Username string
	LoginV2  bool
	password string

	mu  sync.Mutex
	sid string
}

type Option func(*Client) error

// NewClient creates a new client to interact with the AHA interface.
func NewClient(addr, password string, options ...Option) (*Client, error) {
	u, err := url.Parse(addr)
	if err != nil {
		return nil, err
	}
	transport := http.DefaultTransport.(*http.Transport).Clone()
	transport.TLSClientConfig.ServerName = "fritz.box"
	c := &Client{
		Transport: transport,
		URL:       *u,
		password:  password,
	}
	for _, o := range options {
		if err := o(c); err != nil {
			return nil, err
		}
	}
	return c, nil
}

// TrustCertificate adds a PEM certificate to be trusted. Be aware that self-signed certificates may be re-created on reboot. You might prefer to use TrustOnFirstUse.
//
// See also https://en.avm.de/service/knowledge-base/dok/FRITZ-Box-7490/1523_Downloading-your-FRITZ-Box-s-certificate-and-importing-it-to-your-computer/
func TrustCertificate(bs []byte) Option {
	return func(c *Client) error {
		if c.Transport.TLSClientConfig.RootCAs == nil {
			c.Transport.TLSClientConfig.RootCAs = x509.NewCertPool()
		}
		if ok := c.Transport.TLSClientConfig.RootCAs.AppendCertsFromPEM(bs); !ok {
			return errors.New("could not TrustCertificate")
		}
		return nil
	}
}

// TrustOnFirstUse retrieves the certificate and marks it as trusted for subsequent calls.
func TrustOnFirstUse(c *Client) error {
	host := c.URL.Host
	if c.URL.Port() == "" {
		host += ":443"
	}
	conn, err := tls.Dial("tcp", host, &tls.Config{
		InsecureSkipVerify: true, // We'll verify ourselves
	})
	if err != nil {
		return err
	}
	defer conn.Close()

	state := conn.ConnectionState()
	if len(state.PeerCertificates) == 0 {
		return errors.New("no peer certificates")
	}

	if c.Transport.TLSClientConfig.RootCAs == nil {
		c.Transport.TLSClientConfig.RootCAs = x509.NewCertPool()
	}
	for _, cert := range state.PeerCertificates {
		c.Transport.TLSClientConfig.RootCAs.AddCert(cert)
	}
	return nil
}

// WithUsername disables the auto-discovery of the username.
func WithUsername(username string) Option {
	return func(c *Client) error {
		c.Username = username
		return nil
	}
}

// LoginV2 asks the fritzbox for the PBKDF2 challenge on login.
func LoginV2(c *Client) error {
	c.LoginV2 = true
	return nil
}

func (c *Client) joinPath(elem ...string) ahaURL {
	return ahaURL{c.URL.JoinPath(elem...)}
}

type ahaURL struct {
	*url.URL
}

func (a ahaURL) Param(key string, values ...string) ahaURL {
	q := a.Query()
	q[key] = values
	a.RawQuery = q.Encode()
	return a
}

func (c *Client) doSIDReq(req *http.Request) (*http.Response, error) {
	sid, err := c.getSID(false)
	if err != nil {
		return nil, err
	}

	q := req.URL.Query()
	q.Add("sid", sid)
	req.URL.RawQuery = q.Encode()
	client := &http.Client{Transport: c.Transport}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}

	if resp.StatusCode == http.StatusForbidden {
		// retry once on 403 response
		sid, err = c.getSID(true)
		if err != nil {
			return nil, err
		}

		q := req.URL.Query()
		q.Add("sid", sid)
		req.URL.RawQuery = q.Encode()
		resp, err = client.Do(req)
		if err != nil {
			return nil, err
		}
	}

	if resp.StatusCode != http.StatusOK {
		resp.Body.Close()
		return nil, fmt.Errorf("unexpected http status code: %d", resp.StatusCode)
	}
	return resp, nil
}

func (c *Client) doXMLReq(req *http.Request, v any) error {
	client := &http.Client{Transport: c.Transport}
	resp, err := client.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("unexpected http status code: %d", resp.StatusCode)
	}
	if err := xml.NewDecoder(resp.Body).Decode(v); err != nil {
		return fmt.Errorf("could not decode XML: %w", err)
	}
	return nil
}

func (c *Client) getSID(forceRefresh bool) (string, error) {
	c.mu.Lock()
	defer c.mu.Unlock()
	if !forceRefresh && c.sid != "" {
		return c.sid, nil
	}

	u := c.joinPath("login_sid.lua")
	if c.LoginV2 {
		u.RawQuery = "version=2" // ask for pbkdf2 challenge
	}
	req, err := http.NewRequest("GET", u.String(), nil)
	if err != nil {
		return "", err
	}
	var session sessionInfo
	if err := c.doXMLReq(req, &session); err != nil {
		return "", err
	}
	responseAt := time.Now()

	response, err := challengeResponse(session.Challenge, c.password)
	if err != nil {
		return "", err
	}

	q := u.Query()
	q.Add("username", c.getUsername(session))
	q.Add("response", response)
	u.RawQuery = q.Encode()
	req, err = http.NewRequest("GET", u.String(), nil)
	if err != nil {
		return "", err
	}
	if session.BlockTime > 0 {
		d := (time.Duration(session.BlockTime) * time.Second) - time.Since(responseAt)
		time.Sleep(d)
	}
	var session2 sessionInfo
	if err := c.doXMLReq(req, &session2); err != nil {
		return "", err
	}
	if session2.SID == "" || session2.SID == "0000000000000000" {
		return "", errors.New("login failed")
	}
	c.sid = session2.SID
	return c.sid, nil
}

func (c *Client) getUsername(session sessionInfo) string {
	if c.Username != "" {
		return c.Username
	}
	for _, u := range session.Users {
		return u.Text
	}
	return "admin" //fallback to sensible value
}

type sessionInfo struct {
	XMLName   xml.Name `xml:"SessionInfo"`
	SID       string   `xml:"SID"`
	Challenge string   `xml:"Challenge"`
	BlockTime int      `xml:"BlockTime"`
	Rights    string   `xml:"Rights"`
	Users     []struct {
		Text string `xml:",chardata"`
		Last string `xml:"last,attr"`
	} `xml:"Users>User"`
}
