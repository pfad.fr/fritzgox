package aha

import (
	"encoding/xml"
	"fmt"
	"io"
	"net/http"
	"strconv"
	"time"
)

type DeviceFunction uint64

const (
	DeviceHANFUN DeviceFunction = 1 << iota
	_
	DeviceLight
	_
	DeviceAlarm
	DeviceButton
	DeviceThermostat
	DeviceEnergieSensor
	DeviceTemperatureSensor
	DeviceSwitch
)

type Float32Tenth float32

func (f *Float32Tenth) UnmarshalXML(d *xml.Decoder, start xml.StartElement) error {
	var v int32
	if err := d.DecodeElement(&v, &start); err != nil {
		return err
	}
	*f = Float32Tenth(float32(v) / 10)

	return nil
}

func (f Float32Tenth) String() string {
	return strconv.FormatFloat(float64(f), 'f', -1, 32)
}

type UnixSecond struct{ time.Time }

func (u *UnixSecond) UnmarshalXML(d *xml.Decoder, start xml.StartElement) error {
	var v int64
	if err := d.DecodeElement(&v, &start); err != nil {
		return err
	}
	if v != 0 {
		u.Time = time.Unix(v, 0)
	}

	return nil
}

type DeviceListInfos struct {
	XMLName         xml.Name `xml:"devicelist"`
	Version         string   `xml:"version,attr"`
	FirmwareVersion string   `xml:"fwversion,attr"`
	Devices         []Device `xml:"device"`
}

type Device struct {
	Identifier      string         `xml:"identifier,attr"`
	ID              string         `xml:"id,attr"`
	Function        DeviceFunction `xml:"functionbitmask,attr"`
	FirmwareVersion string         `xml:"fwversion,attr"`
	Manufacturer    string         `xml:"manufacturer,attr"`
	Productname     string         `xml:"productname,attr"`
	Present         bool           `xml:"present"`
	Txbusy          bool           `xml:"txbusy"`
	Name            string         `xml:"name"`
	BatteryLow      bool           `xml:"batterylow"`
	BatteryLevel    int            `xml:"battery"`

	TemperatureSensor *TemperatureSensor `xml:"temperature"`
	Thermostat        *Thermostat        `xml:"hkr"`
}

type TemperatureSensor struct {
	Celsius Float32Tenth `xml:"celsius"` // adjusted temperature
	Offset  Float32Tenth `xml:"offset"`  // operated adjustment
}

// GetDeviceListInfos returns the connected smart devices.
func (c *Client) GetDeviceListInfos() (info *DeviceListInfos, err error) {
	u := c.joinPath("webservices", "homeautoswitch.lua").Param("switchcmd", "getdevicelistinfos")
	err = c.get(u.String(), &info)
	return info, err
}

func (c *Client) get(url string, xmlDecode any) error {
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return err
	}
	resp, err := c.doSIDReq(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("unexpected http status code: %d", resp.StatusCode)
	}

	switch v := xmlDecode.(type) {
	case nil:
		return nil
	case *string:
		buf, err := io.ReadAll(resp.Body)
		*v = string(buf)
		return err
	default:
		return xml.NewDecoder(resp.Body).Decode(xmlDecode)
	}
}
