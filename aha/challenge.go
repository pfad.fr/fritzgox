package aha

import (
	"crypto/md5"
	"crypto/sha256"
	"encoding/hex"
	"fmt"
	"strconv"
	"strings"
	"unicode/utf16"

	"golang.org/x/crypto/pbkdf2"
)

func challengeResponse(challenge, password string) (string, error) {
	if strings.HasPrefix(challenge, "2$") {
		return challengePBKDF2Response(challenge, password)
	}
	return challengeMD5Response(challenge, password), nil

}

func challengePBKDF2Response(challenge, password string) (string, error) {
	parts := strings.Split(challenge, "$")
	if len(parts) != 5 {
		return "", fmt.Errorf("expected 5 parts, got %d", len(parts))
	}
	if parts[0] != "2" {
		return "", fmt.Errorf(`expected "2" as first part, got %q`, parts[0])
	}

	hash1, err := pbdkf2HmacSha256([]byte(password), parts[1], parts[2])
	if err != nil {
		return "", fmt.Errorf("could not compute hash1: %w", err)
	}

	hash2, err := pbdkf2HmacSha256(hash1, parts[3], parts[4])
	if err != nil {
		return "", fmt.Errorf("could not compute hash2: %w", err)
	}

	// assemble response
	response := append([]byte(parts[4]), '$')
	return string(append(response, []byte(hex.EncodeToString(hash2))...)), nil
}

func pbdkf2HmacSha256(password []byte, iterDec, saltHex string) ([]byte, error) {
	iter, err := strconv.Atoi(iterDec)
	if err != nil {
		return nil, fmt.Errorf("could not parse iter: %w", err)
	}
	salt, err := hex.DecodeString(saltHex)
	if err != nil {
		return nil, fmt.Errorf("could not parse salt2: %w", err)
	}
	return pbkdf2.Key(password, salt, iter, sha256.Size, sha256.New), nil
}

func challengeMD5Response(challenge, password string) string {
	// md5 von <challenge>-<klartextpassword>
	return challenge + "-" + md5UTF16(challenge+"-"+password)
}

func md5UTF16(s string) string {
	utf16 := toUTF16LE(s)
	m := md5.Sum(utf16)
	return hex.EncodeToString(m[:])
}

func toUTF16LE(s string) []byte {
	b := make([]byte, 0, len(s))
	for _, r := range s {
		if r <= 255 {
			b = append(b, byte(r), byte(r>>8))
		} else {
			r1, r2 := utf16.EncodeRune(r)
			b = append(b, byte(r1), byte(r1>>8), byte(r2), byte(r2>>8))
		}
	}
	return b
}
