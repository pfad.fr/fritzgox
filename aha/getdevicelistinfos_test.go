package aha

import (
	"encoding/json"
	"encoding/xml"
	"net/http"
	"net/http/httptest"
	"testing"

	"gotest.tools/v3/assert"
)

func newServer(t *testing.T, switchcmd map[string]http.HandlerFunc) *httptest.Server {
	mux := http.NewServeMux()
	// mux.Handle("/", http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
	// 	w.WriteHeader(http.StatusNotFound)
	// 	t.Log(r.Method, r.URL.String())
	// }))
	mux.Handle("/webservices/homeautoswitch.lua", http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		cmd := r.URL.Query().Get("switchcmd")
		if handler := switchcmd[cmd]; handler != nil {
			handler(w, r)
		} else {
			t.Log("switchcmd", cmd)
			w.WriteHeader(http.StatusNotFound)
		}
	}))
	mux.Handle("/login_sid.lua", http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		t.Log(r.Method, r.URL.String())
		_, err := w.Write([]byte(`<SessionInfo>
		<SID>8b4994376ab804ca</SID>
		<Challenge>a0fa42bb</Challenge>
		<BlockTime>0</BlockTime>
		<Users>
		<User last="1">fritz1337</User>
		</Users>
		<Rights>
		<Name>NAS</Name>
		<Access>2</Access>
		<Name>App</Name>
		<Access>2</Access>
		<Name>HomeAuto</Name>
		<Access>2</Access>
		<Name>BoxAdmin</Name>
		<Access>2</Access>
		<Name>Phone</Name>
		<Access>2</Access>
		</Rights>
		</SessionInfo>`))
		assert.NilError(t, err)
	}))
	server := httptest.NewServer(mux)
	t.Cleanup(server.Close)
	return server
}

func TestGetDeviceListInfos(t *testing.T) {
	server := newServer(t, map[string]http.HandlerFunc{
		"getdevicelistinfos": func(w http.ResponseWriter, r *http.Request) {
			http.ServeFile(w, r, "testdata/getdevicelistinfos.xml")
		},
	})
	addr := "http://" + server.Listener.Addr().String()

	c, err := NewClient(addr, "s3cr3t")
	assert.NilError(t, err)
	info, err := c.GetDeviceListInfos()
	assert.NilError(t, err)

	m, err := json.MarshalIndent(info.Devices, "", "  ")
	assert.NilError(t, err)
	t.Log(string(m))

	assert.Equal(t, 3, len(info.Devices))
	firstDevice := info.Devices[0]
	assert.Equal(t, DeviceTemperatureSensor|DeviceThermostat, firstDevice.Function)

	assert.Equal(t, Float32Tenth(21.5), firstDevice.TemperatureSensor.Celsius)
	assert.Equal(t, "21.5", firstDevice.TemperatureSensor.Celsius.String())
	assert.Equal(t, Float32Tenth(0), firstDevice.TemperatureSensor.Offset)
	assert.Equal(t, "0", firstDevice.TemperatureSensor.Offset.String())

	assert.Equal(t, ThermostatTemp(21.5), firstDevice.Thermostat.ActualTemp)
	assert.Equal(t, ThermostatTemp(16), firstDevice.Thermostat.TargetTemp)
	assert.Equal(t, ThermostatTemp(16), firstDevice.Thermostat.LowTemp)
	assert.Equal(t, ThermostatTemp(21), firstDevice.Thermostat.ComfortTemp)
	assert.Equal(t, "21", firstDevice.Thermostat.ComfortTemp.String())
	assert.Equal(t, 0, firstDevice.Thermostat.ErrorCode)
	assert.Equal(t, false, firstDevice.Thermostat.WindowOpened)
	assert.Equal(t, true, firstDevice.Thermostat.WindowOpenedUntil.IsZero())

	assert.Equal(t, "2022-09-07 18:00:00 +0200 CEST", firstDevice.Thermostat.NextChange.At.String())
}

func TestTemperatureXML(t *testing.T) {
	type s struct {
		XMLName  xml.Name       `xml:"wrapper"`
		WishTemp ThermostatTemp `xml:"tsoll"`
	}

	for _, c := range []struct {
		value    string
		expected ThermostatTemp
	}{
		{
			value:    "0",
			expected: 0,
		},
		{
			value:    "16",
			expected: 8,
		},
		{
			value:    "17",
			expected: 8.5,
		},
		{
			value:    "55",
			expected: 27.5,
		},
		{
			value:    "56",
			expected: 28,
		},
		{
			value:    "120",
			expected: 60,
		},
		{
			value:    "254",
			expected: 37, // on
		},
		{
			value:    "253",
			expected: 0, // off
		},
		{
			value:    "255",
			expected: -1, // undefined
		},
	} {
		t.Run(c.value, func(t *testing.T) {
			var got s
			err := xml.Unmarshal([]byte("<wrapper><tsoll>"+c.value+"</tsoll></wrapper>"), &got)
			assert.NilError(t, err)
			assert.Equal(t, c.expected, got.WishTemp)
		})
	}

	on := ThermostatTemp(37)
	assert.Check(t, on == ThermostatTempOn)

	off := ThermostatTemp(0)
	assert.Check(t, off == ThermostatTempOff)

	undefined := ThermostatTemp(-1)
	assert.Check(t, undefined == ThermostatTempUndefined)
}
