module code.pfad.fr/fritzgox

go 1.19

require (
	golang.org/x/crypto v0.0.0-20220829220503-c86fa9a7ed90
	gotest.tools/v3 v3.3.0
)

require github.com/google/go-cmp v0.5.5 // indirect
