package upnp_test

import (
	"fmt"

	"code.pfad.fr/fritzgox/upnp"
)

func ExampleClient_ListServicesActionsVariables() {
	c, _ := upnp.NewClient("http://192.168.178.1:49000", nil)
	svc, _ := c.ListServicesActionsVariables()
	for s, actions := range svc {
		fmt.Println(s)
		for a, vars := range actions {
			fmt.Println(" - " + a)
			for _, v := range vars {
				fmt.Println("    " + v)
			}
		}
	}
}
