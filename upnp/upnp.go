// package upnp allows to query the un-authenticated UPnP fritzbox endpoints (with stats regarding the internet connection for instance).
package upnp

import (
	"encoding/xml"
	"fmt"
	"io"
	"net/http"
	"strings"
)

// NewClient creates a new UPnP client and discover the services.
//
// Argument addr should not have a trailing slash (usually http://192.168.178.1:49000).
// If client is nil, http.DefaultClient will be used.
func NewClient(addr string, client *http.Client) (*Client, error) {
	if client == nil {
		client = http.DefaultClient
	}

	var desc igdDesc
	if err := xmlDecoder(client.Get(addr + "/igddesc.xml"))(&desc); err != nil {
		return nil, err
	}

	return &Client{
		BaseURL:    addr,
		Services:   desc.ServicesByType(),
		HTTPClient: client,
	}, nil
}

type Client struct {
	BaseURL    string
	Services   map[string]*service // Services will be populated on "NewClient"
	HTTPClient *http.Client
}

// ListServicesActionsVariables returns a list of services, with associated actions and variable names.
// See Example.
func (c Client) ListServicesActionsVariables() (map[string]map[string][]string, error) {
	services := make(map[string]map[string][]string)
	for name, svc := range c.Services {
		services[name] = make(map[string][]string)

		aan, err := svc.getActionArgumentName(c.BaseURL, c.HTTPClient)
		if err != nil {
			return nil, err
		}
		for action, mapping := range aan {
			variables := make([]string, 0, len(mapping))
			for _, v := range mapping {
				variables = append(variables, v)
			}
			services[name][action] = variables
		}
	}

	return services, nil
}

// GetStrings retrieves the values associated with the given (service, action).
// Call ListServicesActionsVariables to get the list of availables services, actions and variables.
func (c Client) GetStrings(service, action string) (map[string]string, error) {
	svc, ok := c.Services[service]
	if !ok {
		return nil, fmt.Errorf("unknown service: %q", service)
	}

	mapping, err := svc.fetchSCPD(c.BaseURL, c.HTTPClient, action)
	if err != nil {
		return nil, err
	}

	bodystr := fmt.Sprintf(`
        <?xml version='1.0' encoding='utf-8'?>
        <s:Envelope s:encodingStyle='http://schemas.xmlsoap.org/soap/encoding/' xmlns:s='http://schemas.xmlsoap.org/soap/envelope/'>
            <s:Body>
                <u:%s xmlns:u='%s' />
            </s:Body>
        </s:Envelope>
    `, action, service)

	req, err := http.NewRequest("POST", c.BaseURL+svc.ControlURL, strings.NewReader(bodystr))
	if err != nil {
		return nil, err
	}

	req.Header.Set("Content-Type", `text/xml; charset="utf-8"`)
	req.Header.Set("SoapAction", service+"#"+action)
	resp, err := c.HTTPClient.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("unexpected http status code: %d", resp.StatusCode)
	}

	return parseSOAPResponse(resp.Body, mapping, action+"Response")
}

func parseSOAPResponse(r io.Reader, mapping map[string]string, skip ...string) (map[string]string, error) {
	res := make(map[string]string)
	dec := xml.NewDecoder(r)

	skip = append([]string{"Envelope", "Body"}, skip...)

	for {
		t, err := dec.Token()
		if err == io.EOF {
			return res, nil
		} else if err != nil {
			return nil, err
		}

		se, ok := t.(xml.StartElement)
		if !ok {
			continue
		}
		tagName := se.Name.Local
		if len(skip) > 0 && skip[0] == tagName {
			skip = skip[1:]
			continue
		}
		mapped := mapping[tagName]
		if mapped == "" {
			continue
		}

		tok, err := dec.Token()
		if err != nil {
			return nil, err
		}

		var val string
		switch element := tok.(type) {
		case xml.EndElement:
			val = ""
		case xml.CharData:
			val = string(element)
		default:
			return nil, fmt.Errorf("unexpected xml element %T", element)
		}
		res[mapped] = val
	}
}

// xmlDecoder checks the http status and closes the response body
func xmlDecoder(resp *http.Response, err error) func(v any) error {
	return func(v any) error {
		if err != nil {
			return err
		}
		defer resp.Body.Close()
		if resp.StatusCode != http.StatusOK {
			return fmt.Errorf("unexpected http status code: %d", resp.StatusCode)
		}
		if err := xml.NewDecoder(resp.Body).Decode(v); err != nil {
			return fmt.Errorf("could not decode XML: %w", err)
		}
		return nil
	}
}

type igdDesc struct {
	XMLName     xml.Name `xml:"root"`
	Xmlns       string   `xml:"xmlns,attr"`
	SpecVersion struct {
		Major string `xml:"major"`
		Minor string `xml:"minor"`
	} `xml:"specVersion"`
	Device device `xml:"device"`
}

func (d igdDesc) ServicesByType() map[string]*service {
	res := make(map[string]*service)
	d.Device.fillServices(res)
	return res
}

type device struct {
	DeviceType       string `xml:"deviceType"`
	FriendlyName     string `xml:"friendlyName"`
	Manufacturer     string `xml:"manufacturer"`
	ManufacturerURL  string `xml:"manufacturerURL"`
	ModelDescription string `xml:"modelDescription"`
	ModelName        string `xml:"modelName"`
	ModelNumber      string `xml:"modelNumber"`
	ModelURL         string `xml:"modelURL"`
	UDN              string `xml:"UDN"`

	Services []*service `xml:"serviceList>service"`
	Devices  []device   `xml:"deviceList>device"`

	PresentationURL string `xml:"presentationURL"`
}

func (d device) fillServices(res map[string]*service) {
	for _, s := range d.Services {
		res[s.ServiceType] = s
	}
	for _, dd := range d.Devices {
		dd.fillServices(res)
	}
}
