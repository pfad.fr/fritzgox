package upnp

import (
	"encoding/xml"
	"net/http"
	"sync"
)

type service struct {
	ServiceType string `xml:"serviceType"`
	ServiceId   string `xml:"serviceId"`
	ControlURL  string `xml:"controlURL"`
	EventSubURL string `xml:"eventSubURL"`
	SCPDURL     string `xml:"SCPDURL"`

	mu                 sync.Mutex
	actionArgumentName map[string]map[string]string
}

func (s *service) fetchSCPD(baseURL string, client *http.Client, action string) (map[string]string, error) {
	aan, err := s.getActionArgumentName(baseURL, client)
	if err != nil {
		return nil, err
	}

	return aan[action], nil
}

func (s *service) getActionArgumentName(baseURL string, client *http.Client) (map[string]map[string]string, error) {
	s.mu.Lock()
	defer s.mu.Unlock()
	if s.actionArgumentName != nil {
		return s.actionArgumentName, nil
	}
	var xmlResponse scpd
	if err := xmlDecoder(client.Get(baseURL + s.SCPDURL))(&xmlResponse); err != nil {
		return nil, err
	}

	aan := make(map[string]map[string]string)
	for _, action := range xmlResponse.Actions {
		an := make(map[string]string)
		for _, argument := range action.Arguments {
			an[argument.Name] = argument.RelatedStateVariable
		}
		aan[action.Name] = an
	}
	s.actionArgumentName = aan

	return s.actionArgumentName, nil
}

type scpd struct {
	XMLName     xml.Name `xml:"scpd"`
	SpecVersion struct {
		Major string `xml:"major"`
		Minor string `xml:"minor"`
	} `xml:"specVersion"`
	Actions []struct {
		Name      string `xml:"name"`
		Arguments []struct {
			Name                 string `xml:"name"`
			Direction            string `xml:"direction"`
			RelatedStateVariable string `xml:"relatedStateVariable"`
		} `xml:"argumentList>argument"`
	} `xml:"actionList>action"`
	StateVariable []struct {
		Name     string `xml:"name"`
		DataType string `xml:"dataType"`
	} `xml:"serviceStateTable>stateVariable"`
}
