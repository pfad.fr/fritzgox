package upnp

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"gotest.tools/v3/assert"
)

func newServer(t *testing.T, dir string) *httptest.Server {
	mux := http.NewServeMux()
	mux.Handle("/igdupnp/control/WANIPConn1", http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte(`<?xml version="1.0" encoding="utf-8"?>
<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/" s:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">
<s:Body>
<u:GetStatusInfoResponse xmlns:u="urn:schemas-upnp-org:service:WANIPConnection:1">
<NewConnectionStatus>Connected</NewConnectionStatus>
<NewLastConnectionError>ERROR_NONE</NewLastConnectionError>
<NewUptime>1472082</NewUptime>
</u:GetStatusInfoResponse>
</s:Body>
</s:Envelope>`))
	}))
	mux.Handle("/", http.FileServer(http.Dir(dir)))
	server := httptest.NewServer(mux)
	t.Cleanup(server.Close)
	return server
}

func TestRetrieval(t *testing.T) {
	server := newServer(t, "testdata")
	addr := "http://" + server.Listener.Addr().String()

	c, err := NewClient(addr, nil)
	assert.NilError(t, err)

	res, err := c.GetStrings("urn:schemas-upnp-org:service:WANIPConnection:1", "GetStatusInfo")
	assert.NilError(t, err)
	assert.Equal(t, "Connected", res["ConnectionStatus"])

	// again to check cache
	res, err = c.GetStrings("urn:schemas-upnp-org:service:WANIPConnection:1", "GetStatusInfo")
	assert.NilError(t, err)
	assert.Equal(t, "1472082", res["Uptime"])

	// not existing action
	res, err = c.GetStrings("urn:schemas-upnp-org:service:WANIPConnection:1", "NOT EXISTING")
	assert.NilError(t, err)
	assert.Equal(t, 0, len(res))
}
