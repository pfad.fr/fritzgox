// upnp-vmagent sends selected upnp metrics to a victoria-metrics server.
package main

import (
	"bytes"
	"flag"
	"fmt"
	"io"
	"log"
	"net"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"time"

	"code.pfad.fr/fritzgox/upnp"
)

func main() {
	var upnpURL, remoteWriteURL string
	var scrapeInterval time.Duration
	flag.StringVar(&upnpURL, "upnp", "http://192.168.178.1:49000", "UPnP address to query (without trailing slash)")
	flag.StringVar(&remoteWriteURL, "remoteWrite", "", "remote write URL (if empty, print to stdout and quit)")
	flag.DurationVar(&scrapeInterval, "scrapeInterval", time.Minute, "scrape interval")
	flag.Parse()

	if err := run(upnpURL, remoteWriteURL, scrapeInterval); err != nil {
		log.Fatal(err)
	}
}

func run(upnpURL, remoteWriteURL string, scrapeInterval time.Duration) error {
	client, err := upnp.NewClient(upnpURL, nil)
	if err != nil {
		return fmt.Errorf("could not create client: %w", err)
	}

	if remoteWriteURL != "" {
		// wait for remoteWriteURL to be reachable
		err := waitForTCPService(remoteWriteURL, time.Second)
		if err != nil {
			return err
		}
	}

	var buf []byte
	for {
		for _, m := range metrics {
			txt, err := m.OpenMetrics(client)
			if err != nil {
				log.Println(err)
			}
			buf = append(buf, txt...)
		}
		if remoteWriteURL == "" {
			fmt.Print(string(buf))
			return nil
		}

		err := sendToVM(remoteWriteURL, bytes.NewReader(buf))
		if err != nil {
			return fmt.Errorf("could not send to victoria metrics: %w", err)
		}

		buf = buf[:0]
		time.Sleep(scrapeInterval)
	}
}

type metric struct {
	Service, Action string
	Labels          []string
	LabelsMapping   map[string]string
}

var metrics = []metric{
	{
		Service: "urn:schemas-upnp-org:service:WANCommonInterfaceConfig:1",
		Action:  "GetTotalPacketsReceived",
		Labels: []string{
			"TotalPacketsReceived",
		},
		LabelsMapping: map[string]string{
			"TotalPacketsReceived": "upnp_packets_received_total",
		},
	},
	{
		Service: "urn:schemas-upnp-org:service:WANCommonInterfaceConfig:1",
		Action:  "GetTotalPacketsSent",
		Labels: []string{
			"TotalPacketsSent",
		},
		LabelsMapping: map[string]string{
			"TotalPacketsSent": "upnp_packets_sent_total",
		},
	},
	{
		Service: "urn:schemas-upnp-org:service:WANCommonInterfaceConfig:1",
		Action:  "GetAddonInfos",
		Labels: []string{
			"X_AVM_DE_TotalBytesReceived64",
			"TotalBytesReceived",
			"ByteReceiveRate",
			"X_AVM_DE_TotalBytesSent64",
			"TotalBytesSent",
			"ByteSendRate",
		},
		LabelsMapping: map[string]string{
			"X_AVM_DE_TotalBytesReceived64": "upnp_bytes_received_total",
			"X_AVM_DE_TotalBytesSent64":     "upnp_bytes_sent_total",
			"TotalBytesReceived":            "upnp_std_bytes_received_total",
			"TotalBytesSent":                "upnp_std_bytes_sent_total",
			"ByteSendRate":                  "upnp_send_byterate",
			"ByteReceiveRate":               "upnp_receive_byterate",
		},
	},
	{
		Service: "urn:schemas-upnp-org:service:WANCommonInterfaceConfig:1",
		Action:  "GetCommonLinkProperties",
		Labels: []string{
			"Layer1UpstreamMaxBitRate",
			"Layer1DownstreamMaxBitRate",
			"PhysicalLinkStatus",
		},
		LabelsMapping: map[string]string{
			"Layer1UpstreamMaxBitRate":   "upnp_layer1_upstream_max_bitrate",
			"Layer1DownstreamMaxBitRate": "upnp_layer1_downstream_max_bitrate",
			"PhysicalLinkStatus":         "upnp_physical_link_info",
		},
	},
	{
		Service: "urn:schemas-upnp-org:service:WANIPConnection:1",
		Action:  "GetStatusInfo",
		Labels: []string{
			"Uptime",
			"ConnectionStatus",
		},
		LabelsMapping: map[string]string{
			"Uptime":           "upnp_uptime",
			"ConnectionStatus": "upnp_connection_info",
		},
	},

	// Needs authentication
	// {
	// 	Service: "urn:dslforum-org:service:WLANConfiguration:1",
	// 	Action:  "GetTotalAssociations",
	// 	Label:   "TotalAssociations",
	// },
}

func (m metric) OpenMetrics(client *upnp.Client) ([]byte, error) {
	values, err := client.GetStrings(m.Service, m.Action)
	if err != nil {
		return nil, fmt.Errorf("upnp-action failed: %q - %q: %w", m.Service, m.Action, err)
	}

	var buf []byte
	for _, l := range m.Labels {
		label := m.LabelsMapping[l]
		if label == "" {
			return buf, fmt.Errorf("missing label mapping for %q", l)
		}

		v := values[l]
		if strings.HasSuffix(label, "_info") {
			s := label + `{status="` + v + `"} 1` + "\n"
			buf = append(buf, s...)
			continue
		}

		_, err := strconv.ParseUint(v, 10, 64)
		if err != nil {
			log.Printf("invalid int64 value %s: %q. %v\n", l, v, err)
			continue
		}
		s := label + " " + v + "\n"
		buf = append(buf, s...)
	}
	return buf, nil
}

func sendToVM(remoteWriteURL string, body io.Reader) error {
	resp, err := http.Post(remoteWriteURL, "text/plain", body)
	if err != nil {
		return err
	}
	resp.Body.Close()
	if resp.StatusCode >= 400 {
		return fmt.Errorf("unexpected status code: %d", resp.StatusCode)
	}
	return nil
}

func waitForTCPService(rawURL string, delay time.Duration) error {
	u, err := url.Parse(rawURL)
	if err != nil {
		return err
	}
	hostname := u.Hostname()
	port := u.Port()
	if port == "" {
		p, err := net.LookupPort("tcp", u.Scheme)
		if err != nil {
			return err
		}
		port = strconv.Itoa(p)
	}
	waitForTCPDial(hostname+":"+port, time.Second)
	return nil
}

func waitForTCPDial(address string, delay time.Duration) {
	start := time.Now()
	failedOnce := false
	for {
		conn, err := net.DialTimeout("tcp", address, 3*delay)
		if err == nil {
			conn.Close()
			if failedOnce {
				log.Println(address, "dialed successfully")
			}
			return
		}
		failedOnce = true
		log.Println(err)
		elapsed := time.Since(start)
		if elapsed < delay {
			time.Sleep(delay - elapsed)
		}
		start = time.Now()
	}
}
