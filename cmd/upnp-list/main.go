package main

import (
	"fmt"
	"log"

	"code.pfad.fr/fritzgox/upnp"
)

func main() {
	if err := run("http://192.168.178.1:49000"); err != nil {
		log.Fatal(err)
	}
}

func run(addr string) error {
	c, err := upnp.NewClient(addr, nil)
	if err != nil {
		return fmt.Errorf("could not create client: %w", err)
	}
	svc, err := c.ListServicesActionsVariables()
	if err != nil {
		return fmt.Errorf("could not list variables: %w", err)
	}
	for s, actions := range svc {
		fmt.Println(s)
		for a, vars := range actions {
			values, err := c.GetStrings(s, a)
			if err != nil {
				log.Printf("could not get strings for %s[%s]: %v\n", s, a, err)
			}
			fmt.Println(" - " + a)
			for _, v := range vars {
				fmt.Print("    " + v)
				if values[v] != "" {
					fmt.Println(":", values[v])
				} else {
					fmt.Println()
				}
			}
		}
	}
	return nil
}
